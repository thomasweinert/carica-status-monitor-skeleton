<?php

require 'vendor/autoload.php';

use Carica\StatusMonitor\Library as Library;

$url = 'https://bitbucket.org/ThomasWeinert/carica-status-monitor/atom';

$feed = new Library\Feed(
  new Library\Source\Url($url)
);

$feed->output();